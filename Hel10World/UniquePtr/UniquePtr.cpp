// UniquePtr.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>
#include <memory>

struct Foo
{
	Foo() { std::cout << "Foo::Foo\n"; }
	~Foo() { std::cout << "Foo::~Foo\n"; }
	void bar() { std::cout << "Foo::bar\n"; }
};

void f(const Foo &)
{
	std::cout << "f(const Foo&)\n";
}

int main()
{
	std::unique_ptr<Foo> p1(new Foo);  // p1 es propietario de Foo
	if (p1) p1->bar();

	{
		std::unique_ptr<Foo> p2(std::move(p1));  // p2 es ahora propietario de Foo. Un paso directo sin movimiento, falla:
		//std::unique_ptr<Foo> p3 = p1;
		f(*p2);

		p1 = std::move(p2);  // De nuevo p1 es el propietario
		std::cout << "adios p2...\n";
		if (p2 == nullptr)
			std::cout << "p2 es nulo ahora\n";
	}

	if (p1) p1->bar();

	// Foo se destruye al salir de �mbito
}