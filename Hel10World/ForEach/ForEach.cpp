// ForEach.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>

int main()
{
	std::vector<std::wstring> v;
	v.push_back(L"Hola");
	v.push_back(L" ");
	v.push_back(L"Mundo");
	v.push_back(L"\r\n");

	for each(auto str in v)
	{
		std::wcout << str.c_str();
	}

	int x[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	for (int y : x)
	{ 
		std::cout << y << " ";
	}

    return 0;
}

