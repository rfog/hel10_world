// CodigoNuevo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
/*
	1.-	Referencias Ejemplo 1

	strcpy(char *orig,char *dest);
*/
int r_strcpy(char orig[], char dest[])
{
	auto posicion = 0;
	while (orig[posicion] != 0)
		dest[posicion] = orig[posicion++];
	dest[posicion] = '\0';
	return posicion;
}

/*
	1.-	Referencias Ejemplo 2

strcpy(char *orig,char *dest);
*/
int s_strcpy(std::string& orig, std::string& dest)
{
	dest = orig;
	return orig.length();
}

/*
	2.- Sem�ntica de movimiento

*/
class MueveLaCosita
{
	TCHAR *m_pCad;
	size_t m_size;
public:
	MueveLaCosita() :m_pCad(nullptr), m_size(0) {}
	~MueveLaCosita()
	{
		if (m_pCad != nullptr)
			delete[] m_pCad;
		m_size = 0;
	}
	MueveLaCosita(const TCHAR* const orig)
	{
		m_size= _tcslen(orig)+1;
		m_pCad = new TCHAR[m_size];
		_tcscpy_s(m_pCad, m_size, orig);
	}
	MueveLaCosita(const MueveLaCosita& orig) //�Ahiv�patxi, una referencia!
	{
		m_pCad = new TCHAR[orig.m_size];
		_tcscpy_s(m_pCad, orig.m_size,orig.m_pCad);
	}
	MueveLaCosita(MueveLaCosita&& orig)
	{
		m_pCad = orig.m_pCad;
		m_size = orig.m_size;

		orig.m_pCad = nullptr;
		orig.m_size = 0;
	}

	bool EstaVacia()
	{
		return m_pCad == nullptr || m_size == 0;
	}
};

int main()
{
	//1.- Referencias ejemplo 1
	char cad[256];
	r_strcpy("Hola mama", cad); //Ojo con las cadenas hardcodeadas.
	std::cout << cad<<std::endl;

	//1.- Referencias ejemplo 2
	std::string stdCad;
	s_strcpy(std::string("Hola otra vez"), stdCad);
	std::cout << stdCad << std::endl;

    return 0;
}

