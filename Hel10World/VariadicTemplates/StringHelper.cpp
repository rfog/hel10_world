#include "stdafx.h"
#include "StringHelper.h"


StringHelper::StringHelper()
{
}


StringHelper::~StringHelper()
{
}

std::wstring StringHelper::Anchor(int i)
{
	std::wstringstream stream;
	stream << L"{" << i << L"}";
	return stream.str();
}

std::wstring StringHelper::Replace(std::wstring fmtstr, size_t index, const std::wstring& s)
{
	size_t pos = 0;
	std::wstring anchor = Anchor(index);

	while (std::wstring::npos != pos)
	{
		pos = fmtstr.find(anchor, pos);

		if (std::wstring::npos != pos)
		{
			fmtstr.erase(pos, anchor.size());
			fmtstr.insert(pos, s);
			pos += s.size();
		}
	}

	return fmtstr;
}

std::wstring StringHelper::FormatImpl(std::wstring fmt,size_t)
{
	return fmt;
}

std::string StringHelper::Ws2s(const std::wstring wStr)
{
	typedef std::codecvt_utf8<wchar_t> convert_typeX;
	std::wstring_convert<convert_typeX, wchar_t> converterX;

	return converterX.to_bytes(wStr);
}

std::wstring StringHelper::S2ws(const std::string str)
{
	typedef std::codecvt_utf8<wchar_t> convert_typeX;
	std::wstring_convert<convert_typeX, wchar_t> converterX;

	return converterX.from_bytes(str);
}

std::vector<std::wstring> StringHelper::Split(const std::wstring& str, wchar_t delimiter)
{
	std::vector<std::wstring> elems;
	Split(str, delimiter, elems);
	return elems;
}

std::vector<std::wstring> StringHelper::Split(const std::wstring& str, wchar_t delimiter, std::vector<std::wstring>& elems)
{
	std::wstringstream ss(str);
	std::wstring item;
	while (std::getline(ss, item, delimiter)) {
		elems.push_back(item);
	}
	return elems;
}

int StringHelper::wstring2int(std::wstring str)
{
	std::wstringstream ss(str);
	int result;
	return ss >> result ? result : 0;
}

bool StringHelper::IsNullEmptyOrSpace(std::wstring& str)
{
	if (str.length()==0)
		return true;
	for each(auto ch in str)
		if (ch != L' ')
			return false;
	return true;
}

