// VariadicTemplates.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "StringHelper.h"


int main()
{
	std::wstring no = L"no";
	std::wstring me = L"me";
	std::wstring dices = L"dices";
	int hora = 7;
	auto formateado = StringHelper::Format(L"Hola, {0} {1} {2} {0} y son las {3}", no, me, dices, hora);

	std::wcout << formateado <<std::endl;
    return 0;
}

