#pragma once
//#include <boost/property_tree/stream_translator.hpp>

/***
	String utility helper class
*/
class StringHelper
{
	static std::wstring Anchor(int i);
	static std::wstring Replace(std::wstring fmtstr, size_t index, const std::wstring& s);
	//static std::wstring Format(std::wstring fmt, size_t /*index*/) { return fmt; }

	template<typename T, typename... Args>
	static std::wstring FormatImpl(std::wstring fmt, size_t index, T& t, Args&... args);
	static std::wstring FormatImpl(std::wstring fmt, size_t index);
public:
	StringHelper();
	~StringHelper();

	/***
		Gets an UNICODE string and returns the same string in ANSI format
	*/
	static std::string Ws2s(const std::wstring wStr);
	/*
		Gets an ANSI string and returns the same string in UNICODE format
	*/
	static std::wstring S2ws(const std::string str);

	/*
	Returns a vector of tokenized string from delimiters
	*/
	static std::vector<std::wstring> Split(const std::wstring &str, wchar_t delimiter);
	/*
	Returns a vector of tokenized string from delimiters
	*/	static std::vector<std::wstring> Split(const std::wstring &str, wchar_t delimiter, std::vector<std::wstring>& elems);

	static int wstring2int(std::wstring str);

	static bool IsNullEmptyOrSpace(std::wstring& str);

	template<typename T, typename... Args>
	static std::wstring Format(std::wstring fmt, T& t, Args&... args);
};

template<typename T, typename ...Args>
inline std::wstring StringHelper::FormatImpl(std::wstring fmt, size_t index, T& t, Args& ...args)
{
	std::wstringstream stream;
	stream << t;

	std::wstring result = Replace(fmt, index, stream.str());

	++index;

	std::wstring str = FormatImpl(result, index, args...);

	return str;
}

template<typename T, typename ...Args>
inline std::wstring StringHelper::Format(std::wstring fmt, T& t, Args& ...args)
{
	return FormatImpl(fmt, 0, t, args...);
}

