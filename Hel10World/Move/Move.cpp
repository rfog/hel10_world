// Move.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <utility>      // std::move
#include <iostream>     // std::cout
#include <vector>       // std::vector
#include <string>       // std::string


#include <iostream>
#include <algorithm>

class MemoryBlock
{
public:

	MemoryBlock(MemoryBlock&& other)
		: _data(nullptr)
		, _length(0)
	{
		_data = other._data;
		_length = other._length;
		other._data = nullptr;
		other._length = 0;
	}

	// Move assignment operator.
	MemoryBlock& operator=(MemoryBlock&& other)
	{
		std::cout << "In operator=(MemoryBlock&&). length = "
			<< other._length << "." << std::endl;

		if (this != &other)
		{
			// Free the existing resource.
			delete[] _data;

			// Copy the data pointer and its length from the 
			// source object.
			_data = other._data;
			_length = other._length;

			// Release the data pointer from the source object so that
			// the destructor does not free the memory multiple times.
			other._data = nullptr;
			other._length = 0;
		}
		return *this;
	}

	// Simple constructor that initializes the resource.
	explicit MemoryBlock(size_t length)
		: _length(length)
		, _data(new int[length])
	{
		std::cout << "In MemoryBlock(size_t). length = "
			<< _length << "." << std::endl;
	}

	// Destructor.
	~MemoryBlock()
	{
		std::cout << "In ~MemoryBlock(). length = "
			<< _length << ".";

		if (_data != nullptr)
		{
			std::cout << " Deleting resource.";
			// Delete the resource.
			delete[] _data;
		}

		std::cout << std::endl;
	}

	// Copy constructor.
	MemoryBlock(const MemoryBlock& other)
		: _length(other._length)
		, _data(new int[other._length])
	{
		std::cout << "In MemoryBlock(const MemoryBlock&). length = "
			<< other._length << ". Copying resource." << std::endl;

		std::copy(other._data, other._data + _length, _data);
	}

	// Copy assignment operator.
	MemoryBlock& operator=(const MemoryBlock& other)
	{
		std::cout << "In operator=(const MemoryBlock&). length = "
			<< other._length << ". Copying resource." << std::endl;

		if (this != &other)
		{
			// Free the existing resource.
			delete[] _data;

			_length = other._length;
			_data = new int[_length];
			std::copy(other._data, other._data + _length, _data);
		}
		return *this;
	}

	// Retrieves the length of the data resource.
	size_t Length() const
	{
		return _length;
	}

private:
	size_t _length; // The length of the resource.
	int* _data; // The resource.
};


int main()
{
	
	std::string foo = "foo-string";
	std::string bar = "bar-string";
	std::vector<std::string> myvector;

	myvector.push_back(foo);                    // copies
	myvector.push_back(std::move(bar));         // moves

	std::cout << "myvector contains:";
	for (std::string& x : myvector) std::cout << ' ' << x;
	std::cout << '\n';
	
	// Create a vector object and add a few elements to it.

	std::vector<MemoryBlock> v;
	v.push_back(MemoryBlock(25));
	v.push_back(MemoryBlock(75));

	// Insert a new element into the second position of the vector.
	// This operation "moves" the items in the vector instead of copyng them.
	v.insert(v.begin() + 1, MemoryBlock(50));

	return 0;
}

