﻿#pragma once

namespace MathComponent
{
	public ref class IntegerComplex sealed
	{
	public:
		
		IntegerComplex(){}
		IntegerComplex(int real, int imag)
		{
			Real = real;
			Imag = imag;
		}
		property int Real;
		property int Imag;
	};

    public ref class MathObsolete sealed
    {
    public:
		MathObsolete();
		int Suma(int a, int b);
		int Resta(int a, int b);
		int Multiplica(int a, int b);
		int Divide(int a, int b);

		double Suma(double a, double b);
		double Resta(double a, double b);
		double Multiplica(double a, double b);
		double Divide(double a, double b);
		
		IntegerComplex^ Suma(IntegerComplex^ a, IntegerComplex^ b);
		IntegerComplex^ Resta(IntegerComplex^ a, IntegerComplex^ b);
		IntegerComplex^ Multiplica(IntegerComplex^ a, IntegerComplex^ b);
		IntegerComplex^ Divide(IntegerComplex^ a, IntegerComplex^ b);
	
    };
}
