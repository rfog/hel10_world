﻿#include "pch.h"
#include "MathObsolete.h"
#include <MathDll.h>
#include <complex>

using namespace MathComponent;
using namespace Platform;

MathObsolete::MathObsolete()
{
}

int MathComponent::MathObsolete::Suma(int a, int b)
{
	int aa = a, bb = b, resultado;
	::SetDataKindDll(1);
	::Suma(&aa, &bb, &resultado);
	return resultado;
}

int MathComponent::MathObsolete::Resta(int a, int b)
{
	int aa = a, bb = b, resultado;
	::SetDataKindDll(1);
	::Resta(&aa, &bb, &resultado);
	return resultado;
}

int MathComponent::MathObsolete::Multiplica(int a, int b)
{
	int aa = a, bb = b, resultado;
	::SetDataKindDll(1);
	::Multiplica(&aa, &bb, &resultado);
	return resultado;
}

int MathComponent::MathObsolete::Divide(int a, int b)
{
	int aa = a, bb = b, resultado;
	::SetDataKindDll(1);
	::Divide(&aa, &bb, &resultado);
	return resultado;
}

double MathComponent::MathObsolete::Suma(double a, double b)
{
	double aa = a, bb = b, resultado;
	::SetDataKindDll(2);
	::Suma(&aa, &bb, &resultado);
	return resultado;
}

double MathComponent::MathObsolete::Resta(double a, double b)
{
	double aa = a, bb = b, resultado;
	::SetDataKindDll(2);
	::Resta(&aa, &bb, &resultado);
	return resultado;
}

double MathComponent::MathObsolete::Multiplica(double a, double b)
{
	double aa = a, bb = b, resultado;
	::SetDataKindDll(2);
	::Multiplica(&aa, &bb, &resultado);
	return resultado;
}

double MathComponent::MathObsolete::Divide(double a, double b)
{
	double aa = a, bb = b, resultado;
	::SetDataKindDll(2);
	::Divide(&aa, &bb, &resultado);
	return resultado;
}

IntegerComplex^ MathComponent::MathObsolete::Suma(IntegerComplex^ a, IntegerComplex^ b)
{
	auto aa = std::complex<int>(a->Real, a->Imag);
	auto bb = std::complex<int>(b->Real, b->Imag);
	std::complex<int> resultado;

	::SetDataKindDll(3);
	::Suma(&aa, &bb, &resultado);

	return ref new IntegerComplex(resultado.real(), resultado.imag());
}

IntegerComplex^ MathComponent::MathObsolete::Resta(IntegerComplex^ a, IntegerComplex^ b)
{
	auto aa = std::complex<int>(a->Real, a->Imag);
	auto bb = std::complex<int>(b->Real, b->Imag);
	std::complex<int> resultado;

	::SetDataKindDll(3);
	::Resta(&aa, &bb, &resultado);

	return ref new IntegerComplex(resultado.real(), resultado.imag());
}

IntegerComplex^ MathComponent::MathObsolete::Multiplica(IntegerComplex^ a, IntegerComplex^ b)
{
	auto aa = std::complex<int>(a->Real, a->Imag);
	auto bb = std::complex<int>(b->Real, b->Imag);
	std::complex<int> resultado;

	::SetDataKindDll(3);
	::Multiplica(&aa, &bb, &resultado);

	return ref new IntegerComplex(resultado.real(), resultado.imag());
}

IntegerComplex^ MathComponent::MathObsolete::Divide(IntegerComplex^ a, IntegerComplex^ b)
{
	auto aa = std::complex<int>(a->Real, a->Imag);
	auto bb = std::complex<int>(b->Real, b->Imag);
	std::complex<int> resultado;

	::SetDataKindDll(3);
	::Divide(&aa, &bb, &resultado);

	return ref new IntegerComplex(resultado.real(), resultado.imag());
}
