#include "stdafx.h"
#include "MathLib.h"
#include "DataKind.h"	//�Por qu� este debajo del MathKind.h?
#include <complex>
#include <iostream>

MathLib::MathLib()
{
}


MathLib::~MathLib()
{
}

void MathLib::Add(void * a, void * b, void *result)
{
	switch (g_dataKind)
	{
	case 1:
	{
		auto aa = (int *)a;
		auto bb = (int *)b;
		auto cc = *aa + *bb;
		*(int*)result = cc;
	}
	break;
	case 2:
	{
		auto aa = (double *)a;
		auto bb = (double *)b;
		auto cc = *aa + *bb;
		*(double*)result = cc;
	}
	break;
	case 3:
	{
		auto aa = (std::complex<int> *)a;
		auto bb = (std::complex<int> *)b;
		auto cc = *aa + *bb;
		*(std::complex<int>*)result = cc;
	}
	break;
	}
}

void MathLib::Substract(void * a, void * b, void *result)
{
	switch (g_dataKind)
	{
	case 1:
	{
		auto aa = (int *)a;
		auto bb = (int *)b;
		auto cc = *aa - *bb;
		*(int*)result = cc;
	}
	break;
	case 2:
	{
		auto aa = (double *)a;
		auto bb = (double *)b;
		auto cc = *aa - *bb;
		*(double*)result = cc;
	}
	break;
	case 3:
	{
		auto aa = (std::complex<int> *)a;
		auto bb = (std::complex<int> *)b;
		auto cc = *aa - *bb;
		*(std::complex<int>*)result = cc;
	}
	break;
	}
}

void MathLib::Multiply(void * a, void * b, void *result)
{
	switch (g_dataKind)
	{
	case 1:
	{
		auto aa = (int *)a;
		auto bb = (int *)b;
		auto cc = *aa * *bb;
		*(int*)result = cc;
	}
	break;
	case 2:
	{
		auto aa = (double *)a;
		auto bb = (double *)b;
		auto cc = *aa * *bb;
		*(double*)result = cc;
	}
	break;
	case 3:
	{
		auto aa = (std::complex<int> *)a;
		auto bb = (std::complex<int> *)b;
		auto cc = *aa * *bb;
		*(std::complex<int>*)result = cc;
	}
	break;
	}
}

void MathLib::Divide(void * a, void * b, void *result)
{
	switch (g_dataKind)
	{
	case 1:
	{
		auto aa = (int *)a;
		auto bb = (int *)b;
		auto cc = *aa / *bb;
		*(int*)result = cc;
	}
	break;
	case 2:
	{
		auto aa = (double *)a;
		auto bb = (double *)b;
		auto cc = *aa / *bb;
		*(double*)result = cc;
	}
	break;
	case 3:
	{
		auto aa = (std::complex<int> *)a;
		auto bb = (std::complex<int> *)b;
		auto cc = *aa / *bb;
		*(std::complex<int>*)result = cc;
	}
	break;
	}
}

void MathLib::Print(void * a)
{
	switch (g_dataKind)
	{
	case 1:
	{
		auto aa = (int *)a;
		std::cout << *aa;
	}
	break;
	case 2:
	{
		auto aa = (double *)a;
		std::cout << *aa;
	}
		break;
	case 3:
	{
		auto aa = (std::complex<int> *)a;
		std::cout << *aa;
	}
	break;
	}
}
