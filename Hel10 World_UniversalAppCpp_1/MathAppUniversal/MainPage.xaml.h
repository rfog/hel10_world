﻿//
// MainPage.xaml.h
// Declaration of the MainPage class.
//

#pragma once

#include "MainPage.g.h"

namespace MathAppUniversal
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public ref class MainPage sealed
	{
	public:
		MainPage();

	private:
		void EjecutaOperacion_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void DataTypeComboBox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e);

		void CalculateInteger();
		void CalculateDouble();
		void CalculateComplex();

	};
}
