#pragma once

#ifdef MATHDLL_EXPORTS
#define MATHDLL_API __declspec(dllexport)
#else
#define MATHDLL_API __declspec(dllimport)
#endif

MATHDLL_API void Suma(void *a, void *b, void *resultado);

MATHDLL_API void Resta(void *a, void *b, void *resultado);

MATHDLL_API void Multiplica(void *a, void *b, void *resultado);

MATHDLL_API void Divide(void *a, void *b, void *resultado);

/* 

	Cambiando el nombre porque entramos en conflicto con SetDataKind global. 
	Otra chufa m�s. :-)
*/
MATHDLL_API void SetDataKindDll(int dataKind);