#include "pch.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTestApp
{
	//public y sealed para usar con otros lenguajes.
	public ref class ClaseReferencia sealed
	{
	public:
		virtual ~ClaseReferencia()
		{
			OutputDebugString(L"Aaaadios\r\n");
		}
		void DiHola()
		{
			OutputDebugString(L"Hola soy una referencia\r\n");
		}
	};

	value class ClaseValor
	{
	public:
		int Propiedad;
	};

	//public y sealed para usar con otros lenguajes.
	public ref struct StructReferencia sealed
	{
		virtual ~StructReferencia()
		{
			OutputDebugString(L"Aaaadios de esta estructura\r\n");
		}
		void DiHola()
		{
			OutputDebugString(L"Hola soy una estructura referencia\r\n");
		}
	};

	value struct StructValor
	{
		int Propiedad;
	};

    TEST_CLASS(TestAmbito)
    {
    public:
        TEST_METHOD(TestStructClass)
        {
			{
				auto referenca = ref new ClaseReferencia();
				referenca->DiHola();
			}
			ClaseValor valor;
			valor.Propiedad = 33;

			{
				//Mmmmmmmm... igual que la de arriba?
				auto referenca = ref new StructReferencia();
				referenca->DiHola();
			}
			StructValor svalor;
			svalor.Propiedad = 33;

			Assert::IsTrue(true);
        }
    };


	enum class EnumeracionClase//:int 
	{
		Uno=1,
		Dos=2,
		Tres=3,
		Cuatro=4
	};

	enum struct EnumeracionEstructura//:char
	{
		Primero = 1,
		Segundo = 2,
		Tercero = 3,
		Cuarto = 4
	};
	TEST_CLASS(TestAmbito1)
	{
	public:
		TEST_METHOD(TestEnumYJerarqu�as)
		{
			EnumeracionClase eClase;
			EnumeracionEstructura eStruct;

			eClase = EnumeracionClase::Cuatro;
			eStruct = EnumeracionEstructura::Cuarto;
			//if (eClase == eStruct)
			//	Assert::IsTrue(false);

			Assert::IsTrue(true);
		}
	};
}