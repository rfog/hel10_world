﻿//
// MainPage.xaml.cpp
// Implementation of the MainPage class.
//

#include "pch.h"
#include "MainPage.xaml.h"
#include <string>
#include <sstream>

using namespace MathAppUniversal;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::System;


using namespace MathComponent;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

MainPage::MainPage()
{
	InitializeComponent();
}


void MathAppUniversal::MainPage::EjecutaOperacion_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	auto dataType = DataTypeComboBox->SelectedIndex;

	switch (dataType)
	{
	case 0:
		CalculateInteger();
		break;
	case 1:
		CalculateDouble();
		break;
	case 2:
		CalculateComplex();
		break;
	default:
		break;
	}
}

void MathAppUniversal::MainPage::CalculateInteger()
{
	auto operationIndex = OperationComboBox->SelectedIndex;
	
	//Lo siento no he encontrado cómo hacer esto:
	//auto aa = Convert::ToInt32(OperadorATextBox.Text);
	auto a = std::stoi(OperadorATextBox->Text->Data());
	auto b = std::stoi(OperadorBTextBox->Text->Data());
	int result = 0;

	auto math = ref new MathObsolete();
	switch (operationIndex)
	{
	case 0:
		result = math->Suma(a, b);
		break;
	case 1:
		result = math->Resta(a, b);
		break;
	case 2:
		result = math->Multiplica(a, b);
		break;
	case 3:
		result = math->Divide(a, b);
		break;
	default:
		break;
	}
	auto str = std::to_wstring(result);
	ResultTextBock->Text = ref new Platform::String(str.c_str());
}

void MathAppUniversal::MainPage::CalculateDouble()
{
	auto operationIndex = OperationComboBox->SelectedIndex;

	auto a = std::stod(OperadorATextBox->Text->Data());
	auto b = std::stod(OperadorBTextBox->Text->Data());

	double result = 0;

	auto math = ref new MathObsolete();
	switch (operationIndex)
	{
	case 0:
		result = math->Suma(a, b);
		break;
	case 1:
		result = math->Resta(a, b);
		break;
	case 2:
		result = math->Multiplica(a, b);
		break;
	case 3:
		result = math->Divide(a, b);
		break;
	default:
		break;
	}
	auto str = std::to_wstring(result);
	ResultTextBock->Text = ref new Platform::String(str.c_str());
}

void MathAppUniversal::MainPage::CalculateComplex()
{
	auto operationIndex = OperationComboBox->SelectedIndex;

	auto ar = std::stoi(OperadorATextBox->Text->Data());
	auto br = std::stoi(OperadorBTextBox->Text->Data());
	auto ai = std::stoi(OperadorAImagTextBox->Text->Data());
	auto bi = std::stoi(OperadorBImagTextBox->Text->Data());

	IntegerComplex a(ar,ai);
	IntegerComplex b(br,bi);
	IntegerComplex^ result;

	auto math = ref new MathObsolete();
	switch (operationIndex)
	{
	case 0:
		result = math->Suma(%a, %b);
		break;
	case 1:
		result = math->Resta(%a, %b);
		break;
	case 2:
		result = math->Multiplica(%a, %b);
		break;
	case 3:
		result = math->Divide(%a, %b);
		break;
	default:
		break;
	}

	std::wstringstream ss;
	ss << result->Real << L"+" << result->Imag << L"i";
	ResultTextBock->Text = ref new Platform::String(ss.str().c_str());
}


void MathAppUniversal::MainPage::DataTypeComboBox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e)
{
	auto index = DataTypeComboBox->SelectedIndex;
	if (index == 2)
	{
		OperadorAImagTextBox->Visibility = Windows::UI::Xaml::Visibility::Visible;
		OperadorBImagTextBox->Visibility = Windows::UI::Xaml::Visibility::Visible;
	}
	else
	{
		OperadorAImagTextBox->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
		OperadorBImagTextBox->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
	}
}
