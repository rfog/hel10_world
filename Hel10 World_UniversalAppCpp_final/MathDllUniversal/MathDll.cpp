// MathDll.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

#include "MathDll.h"
#include <DataKind.h>
#include <MathLib.h>

MathLib math;

void Suma(void *a, void *b, void *resultado)
{
	math.Add(a, b, resultado);
}

void Resta(void *a, void *b, void *resultado)
{
	math.Substract(a, b, resultado);
}

void Multiplica(void *a, void *b, void *resultado)
{
	math.Multiply(a, b, resultado);
}

void Divide(void *a, void *b, void *resultado)
{
	math.Divide(a, b, resultado);
}

void SetDataKindDll(int dataKind)
{
	SetDataKind(dataKind);
}