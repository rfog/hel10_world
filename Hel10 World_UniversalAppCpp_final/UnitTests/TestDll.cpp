#include "stdafx.h"
#include "CppUnitTest.h"
#include <MathDll.h>
#include <complex>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(TestMathDll)
	{
	public:
		
		TEST_METHOD(TestSuma)
		{
			int ia = 3, ib = 5, iresultado;
			SetDataKindDll(1);
			Suma(&ia, &ib, &iresultado);

			Assert::AreEqual(iresultado, 8);

			double da = 3.0, db = 5.0, dresultado;
			SetDataKindDll(2);
			Suma(&da, &db, &dresultado);

			Assert::AreEqual(dresultado, 8.0);

			auto ca = std::complex<int>(3, 2);
			auto cb = std::complex<int>(5, 6);
			std::complex<int> cresultado;
			SetDataKindDll(3);
			Suma(&ca, &cb, &cresultado);

			std::complex<int> verificador(8,8);
			auto ok = cresultado == verificador;
			Assert::AreEqual(ok, true);
		}

		TEST_METHOD(TestResta)
		{
			int ia = 3, ib = 5, iresultado;
			SetDataKindDll(1);
			Resta(&ia, &ib, &iresultado);

			Assert::AreEqual(iresultado, -2);

			double da = 3.0, db = 5.0, dresultado;
			SetDataKindDll(2);
			Resta(&da, &db, &dresultado);

			Assert::AreEqual(dresultado, -2.0);

			auto ca = std::complex<int>(3, 2);
			auto cb = std::complex<int>(5, 6);
			std::complex<int> cresultado;
			SetDataKindDll(3);
			Resta(&ca, &cb, &cresultado);

			std::complex<int> verificador(-2, -4);
			auto ok = cresultado == verificador;
			Assert::AreEqual(ok, true);
		}

		TEST_METHOD(TestMulti)
		{
			int ia = 3, ib = 5, iresultado;
			SetDataKindDll(1);
			Multiplica(&ia, &ib, &iresultado);

			Assert::AreEqual(iresultado, 15);

			double da = 3.0, db = 5.0, dresultado;
			SetDataKindDll(2);
			Multiplica(&da, &db, &dresultado);

			Assert::AreEqual(dresultado, 15.0);

			auto ca = std::complex<int>(3, 2);
			auto cb = std::complex<int>(5, 6);
			std::complex<int> cresultado;
			SetDataKindDll(3);
			Multiplica(&ca, &cb, &cresultado);

			std::complex<int> verificador(3, 28);
			auto ok = cresultado == verificador;
			Assert::AreEqual(ok, true);
		}

		TEST_METHOD(TestDivide)
		{
			int ia = 15, ib = 5, iresultado;
			SetDataKindDll(1);
			Divide(&ia, &ib, &iresultado);

			Assert::AreEqual(iresultado, 3);

			double da = 15.0, db = 5.0, dresultado;
			SetDataKindDll(2);
			Divide(&da, &db, &dresultado);

			Assert::AreEqual(dresultado, 3.0);

			auto ca = std::complex<int>(15, 10);
			auto cb = std::complex<int>(5, 3);
			std::complex<int> cresultado;
			SetDataKindDll(3);
			Divide(&ca, &cb, &cresultado);

			std::complex<int> verificador(3, 2);
			auto ok = cresultado == verificador;
			Assert::AreEqual(ok, true);
		}
	};
}